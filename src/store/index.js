import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dialog: {
            isShow: false
        }
    },
    mutations: {
        showDialog(state) {
            state.dialog.isShow = true;
        },
        closeDialog(state) {
            state.dialog.isShow = false;
        }
    },
    actions: {

    }
});
