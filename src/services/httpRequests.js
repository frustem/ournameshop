import axios from 'axios';

const HTTP = axios.create({
    // baseURL: 'https://developers.ournameshop.com/',
    baseURL: 'http://localhost:3000/',
    headers: {
        'Content-Type': 'application/json'
        // 'user': 'andre',
        // 'password': '12345678'
    }
});
const user = {
    email: 'andre',
    password: 12345678
};

export function authorization() {
    HTTP.post('local', { user })
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        });
}

export function getLogoCategories() {
    return HTTP.get('categories');
}
